import json
import os
from pprint import pprint
from scipy.optimize import linprog

 # https://wiki.factorio.com/Lua/Entity
 # https://wiki.factorio.com/Lua/Recipe
 # https://wiki.factorio.com/Lua/Game#write_file
 # https://github.com/antropod/factorio/tree/master/dump_recipes


# Intermediate is any item which is not an entity

recipes_file = os.path.join(
  os.environ['APPDATA'],
  'Factorio',
  'script-output',
  'recipes.dat'
)
entities_file = os.path.join(
  os.environ['APPDATA'],
  'Factorio',
  'script-output',
  'entities.dat'
)

recipes = {}
items = {}
entities = {}
nodes_and_edges = []

class Item(dict):
	def __init__(self):
		self.name = ""
		self.created_by_recipes = {}
		self.used_by_recipes = {}
		self.isEntity = False
		
		self.isInput = False
		self.outputAmount = 0
		self.index = -1

	def __hash__(self):
		return hash(repr(self.name))

class Recipe(dict):
	def __init__(self):
		self.name = ""
		self.products = []
		self.ingredients = []
		self.crafting_time = 1.0
		self.productivity = 1.0
		self.category = ""
		self.node = None
		self.needed = False
	def __hash__(self):
		return hash(repr(self.name))

class Entity(dict):
	def __init__(self):
		self.item = None
		self.category = ""

def parse_recipes():

	# Parse JSON
	with open(recipes_file) as rf:
		recipes_in = json.load(rf)

	# Populate dict of recipes
	for r,v in recipes_in.items():
		recipes[r] = Recipe()
		recipes[r].name = r
		recipes[r].crafting_time = v['energy']
		recipes[r].category = v['category'] or "crafting"
		
		# Read ingredients
		for i in v['ingredients']:
			ingredient = {}
			ingredient['item'] = i['name']
			ingredient['quantity'] = i['amount']
			recipes[r].ingredients.append(ingredient)
		# Read products
		for p in v['products']:
			product = {}
			product['item'] = p['name']
			product['quantity'] = p['amount']
			recipes[r].products.append(product)

def parse_items():
	for _,r in recipes.items():
		for i in r.ingredients:
			# Create item
			if not i['item'] in items:
				items[i['item']] = Item()
				items[i['item']].name = i['item']

			items[i['item']].used_by_recipes[r.name] = {}

		for p in r.products:
			# Create item
			if not p['item'] in items:
				items[p['item']] = Item()
				items[p['item']].name = p['item']

			items[p['item']].created_by_recipes[r.name] = {}

def parse_entities():
	# Parse JSON
	with open(entities_file) as ef:
		entities_in = json.load(ef)

	# Populate dict of entities
	for r,v in entities_in.items():
		entities[r] = Entity()
		entities[r].name = r
		entities[r].type = v['type']
		if entities[r].name in items:
			items[entities[r].name].isEntity = True

def put_modules_in_recipes():
	for i,r in recipes.items():
		intermediate_product = True
		for p in r.products:
			if items[p['name']].isEntity == True:
				intermediate_product = False

		if intermediate_product == True:
			if r.category == "crafting":
				r.productivity = 1.4
			elif r.category == "smelting":
				r.productivity = 1.2
			elif r.category == "chemistry":
				r.productivity = 1.2
			else:
				r.productivity = 1.0
		else:
			r.productivity = 1.0

def create_dummy_recipes(items,outputs):
	# Create dummy recipes for things that cannot be crafted
	for i,k in enumerate(items):
		if len(items[k].created_by_recipes) == 0:
			dummy_recipe = Recipe()
			dummy_recipe.name = "input:"+items[k].name
			dummy_recipe.crafting_time = 1.0
			dummy_recipe.products.append ( {'item':items[k].name, 'quantity':1 } )
			recipes[dummy_recipe.name] = dummy_recipe
			print ("Dummy recipe created: " + dummy_recipe.name)

	# Create dummy recipes for outputs
	for i,k in enumerate(outputs):
		dummy_recipe = Recipe()
		dummy_recipe.name = "output:"+k['name']
		dummy_recipe.ingredients.append( { 'item':k['name'], 'quantity':k['quantity'] } )
		dummy_recipe.crafting_time = 1.0
		recipes[dummy_recipe.name] = dummy_recipe

	# Create dummy recipes for void outputs with a high penalty cost
	for i,k in enumerate(items):
		dummy_recipe = Recipe()
		dummy_recipe.name = "void-output:"+items[k].name
		dummy_recipe.ingredients.append( { 'item':items[k].name, 'quantity':1 } )
		dummy_recipe.crafting_time = 1.0
		recipes[dummy_recipe.name] = dummy_recipe

# A node representes a recipe which is connected by input and output edges which should be constrained
class Node(dict):
	nodes = {}
	def __init__(self,recipe):
		self.recipe = recipe
		self.name = "Node: " + recipe.name
		self.edges_supply = []   	# edges which supply ingredients to this node
		self.edges_consume = []		# edges which consume products of this node
		self.isOutput = False
		self.isInput = False
		self.bounds = (0, None)
		self.cost = 0.0
		Node.nodes[recipe] = self

# An edge is a link between two nodes, it has a unique index in the optimization matrix and should be used by constraints
class Edge(dict):
	edges = []
	def __init__(self,item):
		self.index = -1
		self.item = item
		self.name = "Edge: " + item.name
		self.consume_node = None
		self.produce_node = None
		self.bounds = (0, None)
		self.cost = 0.0
		Edge.edges.append(self)
	def __hash__(self):
		return hash(repr(self.item.name + self.consume_node.recipe.name + self.produce_node.recipe.name))

class Constraint(dict):
	constraints = []
	def __init__(self,name):
		self.name = name
		self.index = []
		self.values = []
		self.rhs = 0.0
		Constraint.constraints.append(self)
	def AddIndexAndValue(self, index, value):
		self.index.append(index)
		self.values.append(value)

def MakeGraph():

	# Create a node for each recipe
	count = 0
	for i,r in enumerate(recipes):
		if recipes[r].needed == True:
			node = Node(recipes[r])
			node.index = count
			count=count+1
			nodes_and_edges.append(node)
			recipes[r].node = node
			if recipes[r].name.startswith("void-output:"):
				node.cost = 1e-5

		
	# Create and connect edges
	for n in Node.nodes:
		node = Node.nodes[n]

		# Ingredient supply
		for i in node.recipe.ingredients:
			ingredient = items[i['item']]
			for supply_recipe in ingredient.created_by_recipes:
				supply_node = recipes[supply_recipe].node

				if supply_node != None:
					# Edge must be created between (node) and (supply_node)
					edge = Edge(ingredient)
					edge.consume_node = node
					edge.produce_node = supply_node
					#print ("Edge created: " + ingredient.name + " from " + supply_node.recipe.name + " to " + node.recipe.name)

		# Product consumption
		for i in node.recipe.products:
			product = items[i['item']]
			for consume_recipe in product.used_by_recipes:
				consume_node = recipes[consume_recipe].node

				if consume_node != None:
					# Edge must be created between (node) and (consume_node)
					edge = Edge(product)
					edge.consume_node = consume_node
					edge.produce_node = node
					#print ("Edge created: " + product.name + " from " + node.recipe.name + " to " + consume_node.recipe.name)

	# Check for and remove duplicate edges
	new_edges = list(set(Edge.edges))
	Edge.edges = new_edges

	num_nodes = len(Node.nodes)
	num_edges = len(Edge.edges)

	for i,e in enumerate(Edge.edges):
		e.index = i + num_nodes
		nodes_and_edges.append(e)
		e.consume_node.edges_supply.append(e)
		e.produce_node.edges_consume.append(e)
		#print (str(e.index) + ": " + e.item.name + " produced by " + e.produce_node.recipe.name + " (" + str(e.produce_node.index) + ")" + ", consumed by " + e.consume_node.recipe.name + " (" + str(e.consume_node.index) + ")")

	print( "Graph created with " + str(num_nodes) + " nodes and " + str(num_edges) + " edges." )

def CreateOptimizationConstraints():

	for _,n in Node.nodes.items():
		# Output from every node to every edge must be constrained to match its output rate, for each produced item
		for p in n.recipe.products:
			item = items[p['item']]
			qty = p['quantity']
			constraint = Constraint(item.name + " output from " + n.recipe.name)
			constraint.AddIndexAndValue( n.index, + qty ) # Quantity of items produced per craft, productivity goes here
			for e in n.edges_consume:
				if e.item.name == item.name:
					constraint.AddIndexAndValue( e.index, -1 ) # Items consumed by edge


		# Input to every node from each edge must be constrained to match its input ratios and production rate
		for i in n.recipe.ingredients:
			item = items[i['item']]
			qty = i['quantity']
			constraint = Constraint( str(n.recipe.name) + " recipe production requires " + str(qty) + " x " + str(item.name) + "." )
			constraint.AddIndexAndValue(n.index, -qty ) # Quantity of an ingredient required per craft
			for e in n.edges_supply:
				if e.item.name == item.name:
					constraint.AddIndexAndValue (e.index, 1 ) # Items supplied by edge

		if n.recipe.name.startswith("output:"):
			n.isOutput = True
			constraint = Constraint( "Fixing production of " + n.recipe.name + "." )
			n.bounds = ( 1, None )
			constraint.rhs = 1
			constraint.AddIndexAndValue( n.index, 1)
			n.cost = 0

		if n.recipe.name.startswith("input:"):
			n.isInput = True
			constraint = Constraint( "Fixing input of " + n.recipe.name + "." )
			constraint.AddIndexAndValue( n.index, 1)
			n.cost = 1.0
			if n.recipe.name == "input:water":
				n.cost = 0.0
			constraint.rhs = 1e99

def Optimize():
	N = len(Constraint.constraints)
	M = len(nodes_and_edges)
	A = []
	Aineq = []
	b = []
	bineq = []
	bounds = []
	c = []


	for constraint in Constraint.constraints:
		#print("Constraint: " + str(constraint.name))
		row = []
		for i in range(M):
			row.append(0)
		for i in range(len(constraint.index)):
			row[ constraint.index[i] ] = constraint.values[i]

		if (constraint.name.startswith("Fixing input")):
			Aineq.append(row)
			bineq.append(constraint.rhs)
		else:
			A.append(row)
			b.append(constraint.rhs)

	for i in range(M):
		c.append(nodes_and_edges[i].cost)
		bounds.append(nodes_and_edges[i].bounds)


	#pprint(A)
	#pprint(Aineq)

	result = linprog( c, A_ub = Aineq, b_ub = bineq, A_eq = A, b_eq = b, bounds=bounds, options={"tol":1e-7} )
	print(result.message)

	#for i,n in enumerate(Node.nodes):
	#	print( Node.nodes[n].recipe.name +":\t"+ str(result.x[i])).expandtabs(30)

	for i,ne in enumerate(nodes_and_edges):
		if result.x[i] > 1e-10:
			if ne.name.startswith("Node:"):
				print (str(i)+ ": " + str(result.x[i]) + " (" + ne.name + ")")




def recipe_enable_self_and_ingredients(recipe):
	recipe.needed = True
	print("Enabling recipe:\t" + recipe.name)
	for _,i in enumerate(recipe.ingredients):
		for _,r in enumerate(items[i['item']].created_by_recipes):
			recipe_enable_self_and_ingredients(recipes[r])
			#recipe_enable_child_of_multiproduct_recipe(recipes[r])
	for _,i in enumerate(recipe.products):
		recipes["void-output:"+i['item']].needed = True


def recipe_enable_child_of_multiproduct_recipe(recipe):
	if len(recipe.products) > 1:
		recipe.needed = True
		for _,i in enumerate(recipe.products):
			for _,r in enumerate(items[i['item']].used_by_recipes):
				recipe_enable_child_of_multiproduct_recipe(recipes[r])


def main():
	

	outputs = [ {'name':'rocket-part', 'quantity':100}]

	disable_recipes = [	{'name':'empty-crude-oil-barrel'},	{'name':'snow-smelting'}, {'name':'MOX-fuel'}, {'name':'fast-breeder'} ]

	enabled_recipes = [	"basic-oil-processing", "light-oil-cracking", "heavy-oil-cracking", "solid-fuel-from-light-oil", "solid-fuel-from-heavy-oil", "solid-fuel-from-petroleum-gas" ]

	parse_recipes()



	for i,k in enumerate(disable_recipes):
		del recipes[k['name']]
	#for k in recipes.keys():
	#	if len(recipes[k].ingredients)  == 0:
	#		print(recipes[k].name)
	#		del recipes[k]
	#	if recipes[k].name not in enabled_recipes:
	#		del recipes[k]


	parse_items()

	#print(items['processing-unit'].used_by_recipes)
	
	create_dummy_recipes(items,outputs)
	parse_items()
	parse_entities()

	# The process can be dramatically sped up by removing all un-needed recipes, this can be done by working top-down from the output to determine all possible input paths
	for i,k in enumerate(outputs):
		recipe = recipes["output:"+k['name']]
		recipe_enable_self_and_ingredients(recipe)

	MakeGraph()
	CreateOptimizationConstraints()
	Optimize()


main()